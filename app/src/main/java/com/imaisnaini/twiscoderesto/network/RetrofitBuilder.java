package com.imaisnaini.twiscoderesto.network;

import android.content.Context;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;

import static retrofit2.converter.gson.GsonConverterFactory.create;

public class RetrofitBuilder {
    public static Retrofit builder(final Context ctx){
        OkHttpClient.Builder okhttpBuilder = new OkHttpClient().newBuilder();

        okhttpBuilder.connectTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.writeTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.readTimeout(60, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .client(okhttpBuilder.build())
                .addConverterFactory(create())
                .build();

        return retrofit;
    }
}
