package com.imaisnaini.twiscoderesto.network;

public class Config {
    public static final String BASE_URL = "https://themealdb.com";
    public static final String API_URL = BASE_URL + "/api/json/v1/1";
    public static final String API_LIST = API_URL + "/filter.php?c=Seafood";
    public static final String  API_MEAL = API_URL + "/lookup.php";
}
