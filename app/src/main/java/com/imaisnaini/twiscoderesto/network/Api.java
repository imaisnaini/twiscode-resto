package com.imaisnaini.twiscoderesto.network;

import com.imaisnaini.twiscoderesto.network.model.Meals;
import com.imaisnaini.twiscoderesto.network.model.MealList;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {
    @GET(Config.API_MEAL)
    Call<Meals> getMealID(
            @Query("i") String parameter
    );

    @GET(Config.API_LIST)
    Call<MealList> getMeals();
}
