package com.imaisnaini.twiscoderesto;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.imaisnaini.twiscoderesto.network.Api;
import com.imaisnaini.twiscoderesto.network.RetrofitBuilder;
import com.imaisnaini.twiscoderesto.network.model.MealList;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.carousel_MainActiity)
    CarouselView carouselView;
    @BindView(R.id.rvContent_MainActivity)
    RecyclerView rvContent;

    private int[] banner = {R.drawable.banner1, R.drawable.banner2, R.drawable.banner3};
    private Api mApi;
    private List<MealList.Meal> mealList = new ArrayList<>();
    private ListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initListener();
        initViews();
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        new DoSync(this).execute();
    }

    private void initListener(){
        ImageListener listener = new ImageListener() {
            @Override
            public void setImageForPosition(int position, ImageView imageView) {
                imageView.setImageResource(banner[position]);
            }
        };

        carouselView.setPageCount(banner.length);
        carouselView.setImageListener(listener);
    }

    private void initViews(){
        mAdapter = new ListAdapter(mealList, this);
        rvContent.setHasFixedSize(true);
        rvContent.setLayoutManager(new GridLayoutManager(this,2));
        rvContent.setAdapter(mAdapter);
        mAdapter.generate(mealList);
    }

    private class DoSync extends AsyncTask<Void, Void, Void>{
        private final Context context;
        private ProgressDialog dialog;

        private DoSync(Context context){
            this.context = context;
            dialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setTitle("Loading");
            dialog.setMessage("Downloading data..");
            dialog.show();
        }
        @Override
        protected Void doInBackground(Void... voids) {

            mApi.getMeals().enqueue(new Callback<MealList>() {
                @Override
                public void onResponse(Call<MealList> call, Response<MealList> response) {
                    List<MealList.Meal> list = response.body().getMeals();
                    for (MealList.Meal meal : list){
                        mealList.add(meal);
                    }
                    mAdapter.generate(mealList);
                }

                @Override
                public void onFailure(Call<MealList> call, Throwable t) {
                    Toast.makeText(context, "Gagal ambil data", Toast.LENGTH_SHORT).show();
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
            mAdapter.generate(mealList);
        }
    }
}
