package com.imaisnaini.twiscoderesto;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.imaisnaini.twiscoderesto.network.Api;
import com.imaisnaini.twiscoderesto.network.RetrofitBuilder;
import com.imaisnaini.twiscoderesto.network.model.MealList;
import com.imaisnaini.twiscoderesto.network.model.Meals;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {
    @BindView(R.id.tvTitle_detailActivity)
    TextView tvTitle;
    @BindView(R.id.tvDetail_detailActivity)
    TextView tvDetail;
    @BindView(R.id.ivIcon_detailActivity)
    ImageView ivIcon;

    private String idMeal;
    private Api mApi;
    private Meals.Meal meal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        idMeal = String.valueOf(getIntent().getStringExtra("idMeal"));
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        init();
    }

    private void init(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mApi.getMealID(idMeal).enqueue(new Callback<Meals>() {
            @Override
            public void onResponse(Call<Meals> call, Response<Meals> response) {
                List<Meals.Meal> list = response.body().getMeals();
                for (Meals.Meal item : list){
                    meal = item;
                }
                tvTitle.setText(meal.getStrMeal());
                tvDetail.setText(meal.getStrInstructions());
                Picasso.get().load(meal.getStrMealThumb()).into(ivIcon);
            }

            @Override
            public void onFailure(Call<Meals> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Gagal ambil data", Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
