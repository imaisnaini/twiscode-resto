package com.imaisnaini.twiscoderesto;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imaisnaini.twiscoderesto.network.model.MealList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private List<MealList.Meal> list = new ArrayList<>();
    Context ctx;

    public ListAdapter(List<MealList.Meal> list) {
        this.list = list;
    }

    public ListAdapter(List<MealList.Meal> list, Context ctx) {
        this.list = list;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvTitle.setText(list.get(position).getStrMeal());
        Picasso.get().load(list.get(position).getStrMealThumb()).into(holder.ivIcon);
        holder.idMeal = list.get(position).getIdMeal();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tvTitle_itemList)
        TextView tvTitle;
        @BindView(R.id.ivIcon_itemList)
        ImageView ivIcon;
        private String idMeal;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.cvLayout_itemList) void onClick(){
            Intent intent = new Intent(ctx, DetailActivity.class);
            intent.putExtra("idMeal", idMeal);
            ctx.startActivity(intent);
        }
    }

    public void generate(List<MealList.Meal> list){
        this.list = list;
        notifyDataSetChanged();
    }
}
